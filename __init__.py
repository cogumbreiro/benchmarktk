#!/usr/bin/env python
import json
import os
import sys
from itertools import product
import subprocess

class BenchmarkError(Exception):
    pass

class Benchmark(object):
    def __init__(self, opts):
        self.opts = opts
    
    def get(self, *args, **kwargs):
        return self.opts.get(*args, **kwargs)

    def __getitem__(self, key):
        return self.opts[key]
    
    def get_variables(self):
        return self.get('variables', {})

    def get_benchmarks(self):
        """
        Returns a sequence with the available benchmarks.
        """
        return self.get('benchmarks', [])

    def get_run_range(self, default=30):
        """
        Returns the sequence of laps numbers for a given run count.
        """
        return range(1, self.get_run_count(default) + 1)

    def get_run_count(self, default=30):
        """
        The number of times each benchmark should run.
        """
        return self.get('run_count', default)

    def get_parameter_names(self):
        """
        Returns a list of parameter names.
        This list cannot contain the items 'version' and 'benchmark';
        these items will be removed if present.
        """
        result = list(self.get('parameters', ()))
        if 'version' in result:
            result.remove('version')
        if 'benchmark' in result:
            result.remove('benchmark')
        return result
    
    def get_parameter_range(self, param, benchmark, default=None):
        """
        The contents of a parameter should always be an iterable.
        Scalars are returned inside a single-sized tuple.
        In case of None or undefined, it returns an empty tuple.
        """
        def_value = self.get('default_' + param, default)
        all_values = self.get(param, {})
        result = all_values.get(benchmark, def_value)
        try:
            getattr(result, "__iter__")
            return result
        except AttributeError:
            if result is None:
                return ()
            return (result,)
        
    def get_parameters(self, benchmark):
        """
        Obtains all parameter combinations available for a given
        benchmark. Each item of the iterator is a dictionary that ranges from
        parameter names into parameter values.
        """
        names = self.get_parameter_names()
        all_values = [self.get_parameter_range(p, benchmark, default=()) \
                      for p in names]
        for row in product(*all_values):
            yield dict(zip(names, row))

    def get_versions(self):
        return self.opts.get('versions', ())

    def _load_version_data(self, version, params):
        fmt = self.get('version_data_format')
        params['version'] = version
        filename = self.format(fmt, params)
        try:
            with open(filename) as fp:
                data = json.load(fp)
                data['params'] = params
                return data
        except Exception as e:
            raise ValueError(e, filename, "Could not open stats file: %s" % filename)
        
    def _load_diff_benchmark_data(self, benchmark):
        """
        Each row contains:
            std, comparable, ratio, margin_error, confidence_coefficient, mean, params, $version1, $version2
        """
        for params in self.get_parameters(benchmark):
            fmt = self.get('diffs_data_format')
            params['benchmark'] = benchmark
            idx = 1
            for ver in self.get_versions():
                params['version' + str(idx)] = ver
                idx += 1
            filename = self.format(fmt, params)
            with open(filename) as fp:
                data = json.load(fp)
                ver1, ver2 = self.get_versions()
                ver1_data = self._load_version_data(ver1, params)
                ver2_data = self._load_version_data(ver2, params)
                data['params'] = params
                data[ver1] = ver1_data
                data[ver2] = ver2_data
                yield data
    
    def load_data_for_version(self, benchmark, version):
        for params in self.get_parameters(benchmark):
            params['benchmark'] = benchmark
            data = self._load_version_data(version, params)
            data['params'] = params
            yield data

    def load_data(self):
        result = dict()
        for benchmark in self['benchmarks']:
            result[benchmark] = list(self._load_diff_benchmark_data(benchmark))
        return result

    def load_data_by(self, param):
        """
        Data is organized by benchmark.
        Invoking 'result.keys()' will return the available benchmarks.
        Each benchmark holds a list of data:
            std, comparable, ratio, margin_error, confidence_coefficient, mean, params, $version1, $version2        
        """
        result = {}
        for benchmark in self['benchmarks']:
            for data in self._load_diff_benchmark_data(benchmark):
                try:
                    value = data['params'][param]
                except KeyError:
                    msg = "Expected %r, available: %r"
                    msg %= (param, data['params'].keys())
                    raise ValueError(msg)
                prev = result.get(value, {})
                prev[benchmark] = data
                result[value] = prev
        return result

    def is_verbose(self):
        return self.get('verbose', False)

    def is_dry_run(self):
        return self.get('dry_run', False)

    def system(self, cmd):
        """
        Executes a command.
        """
        if self.is_dry_run() or self.is_verbose():
            print(cmd)
        if not self.is_dry_run(): # can run
            return os.system(cmd)
        return 0

    def print_warning(self, msg):
        if self.is_verbose():
            print(msg, file=sys.stderr)

    def system_output(self, cmd):
        if self.is_dry_run() or self.is_verbose():
            print(cmd)
        if not self.is_dry_run(): # can run
            return subprocess.getstatusoutput(cmd)
        return None

    def safe_exec_output(self, cmd):
        """
        Executes a command. Raises a BenchmarkError if execution fails.
        Returns the output. If running in dry-mode, then returns None
        """
        res = self.system_output(cmd)
        if res is not None:
            status, txt = res
            if status != 0:
                raise BenchmarkError("Execution failed: " + cmd)
            return txt
        return None

    def safe_exec(self, cmd):
        """
        Executes a command. Raises a BenchmarkError if execution fails.
        """
        if self.system(cmd) != 0:
            raise BenchmarkError("Execution failed: " + cmd)

    def format(self, term, kwargs):
        try:
            return term.format(**kwargs)
        except KeyError as e:
            msg = f"Error: unknown variable {e}\n"
            msg += f" * Caused by formatting string: {term!r}\n"
            msg += f" * Available variables: {kwargs!r}"
            raise BenchmarkError(msg)

    

def inplace_recursive_update(source, target, copy=lambda o,n:n):
    """
    Recursively updates the source dictionary with the target one.
    Calls 'copy(orig, new_value)' to non-dictionary values, where
    'orig' is None when undefined. By default the copy function
    returns 'new_value', thereby shallow-copying the new value.
    """
    for key, new_value in target.iteritems():
        orig = source.get(key, None)
        if isinstance(orig, dict) and isinstance(new_value, dict):
            inplace_recursive_update(orig, new_value)
        else:
            source[key] = copy(orig, new_value)
            
def yaml_load_all(*files):
    """
    Loads multiple configuration files by merging them together.
    """
    import yaml
    files = list(files)
    opts = yaml.safe_load(files[0])
    del files[0]
    for f in files:
        new_opts = yaml.load(f)
        inplace_recursive_update(opts, new_opts)
    return opts
